#!/bin/bash

clear
files=`git diff-tree --no-commit-id --name-status -r 914c942f1bda90e8b2f70a260e50f66644eef46b`
output="D:/Development/Cypress_Doc/Test/gitlab2"
input="D:/Development/Cypress_Doc/Test/gitlab"
isfilename=false
action=""
filetype=""
echo $files
for file in $files
do
if [ "$file" == "D" ]
then
printf "print0:$file\n"
isfilename=true
action="rm"
filetype="D"
else
if [[ $isfilename && "$action" == "rm" && "$filetype" == "D" ]]
then
printf "print1:$action $output/$file\n"
$action "-rfv" $output/$file
isfilename=false
action=""
filetype=""
fi
fi
if [[ "$file" == "M" ]]
then
printf "print2:$file\n"
isfilename=true
action="cp"
filetype="M"
else
if [[ $isfilename && "$filetype" == "M" ]]
then
printf "print3:$action $input/$file $output/\n"
$action -r $input/$file $output/$file
isfilename=false
action=""
filetype=""
fi
fi
if [[ "$file" == "A" ]]
then
printf "print4:$file\n"
isfilename=true
action="cp"
filetype="A"
else
if [[ $isfilename && "$filetype" == "A" ]]
then
folderpath=$(dirname $file)
printf "print5:$folderpath\n"
mkdir -p $output/$folderpath
printf "print6:$action $input/$file $output/$folderpath\n"
$action $input/$file $output/$folderpath/
isfilename=false
action=""
filetype=""
fi
fi
done


#cd $output
#git add .
#git commit -m "auto commiting changes"
#git push -u "https://gitlab.com/Sadhanaadavelli/rapidiot2.git" master
